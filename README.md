# Divers
Scripts divers de gestion d'un poste client Gnu/Linux

# Sauvegarde d'un /home sur un support externe (disque dur ou partage distant)
Le script sauvegarde_home.sh fonctionne avec exclude_sauvegarde.txt

# Installation de Filius sur un Ubuntu 14.04 qui nécessite Java 8 minimum
