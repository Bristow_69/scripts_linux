#!/bin/bash
# version 1.0

# Ce script sert à installer Filius sur Ubuntu 14.04
# https://www.lernsoftware-filius.de/Herunterladen


#############################################
# Run using sudo, of course.
#############################################
if [ "$UID" -ne "0" ] ; then
  echo "Il faut etre root pour executer ce script. ==> sudo "
  exit 
fi

# Mise à jour des dépôt
apt update

#suppression de java6
apt -y purge openjdk-6*

# Ajout du dépot Java
add-apt-repository -y ppa:openjdk-r/ppa

#Mise à jour et installation
apt update ; apt install -y openjdk-8-jre

#Mise par défaut de la v8
#update-alternatives --config java

#choix auto de Java8 #A tester
update-java-alternatives -s java-1.8.0-openjdk-amd64

#téléchargement de Filius
wget http://www.lernsoftware-filius.de/downloads/filius_1.7.2_all.deb

#installation
chmod +x filius_1.7.2_all.deb
dpkg -i filius_1.7.2_all.deb

#mise à jour totale
apt -y full-upgrade

#echo
echo "Le script Install de Filius a terminé son travail"

read -p "Voulez-vous redémarrer immédiatement ? [O/n] " rep_reboot
if [ "$rep_reboot" = "O" ] || [ "$rep_reboot" = "o" ] || [ "$rep_reboot" = "" ] ; then
  reboot
fi
