#!/bin/bash
REPERTOIRE_SOURCE="/home/login"
REPERTOIRE_DESTINATION="/media/sauvegarde"
  
# Détecter la présence du volume de destination et interrompre l'opération si nécessaire
if [ ! -e "$REPERTOIRE_DESTINATION" ]
then
echo "Attention, le disque de sauvegarde n'est pas présent ou le point de montage non monté"
exit
fi

# Backup des fichiers en excluant tous les dossiers et fichiers contenus dans le fichier exclude_sauvegarde.txt, 
# On filtre aussi les fichiers .iso, .ISO, .mp3...
# A adapter !
rsync -a --progress --delete --stats --exclude-from 'exclude_sauvegarde.txt' --filter "- *.iso" --filter "- *.ISO" --filter "- *.mp3" --filter "- *.avi" --filter "- *.mp4" --filter "- *.tmp" "$REPERTOIRE_SOURCE" "$REPERTOIRE_DESTINATION"
  
echo "La commande de sauvegarde a terminé son travail..."
echo "Vérifiez dans le terminal s'il n'y a pas eu d'erreur !"
read
